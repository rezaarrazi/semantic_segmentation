import argparse
import cv2
import matplotlib.pyplot as plt
import os,sys
import tensorflow as tf

os.environ['SM_FRAMEWORK'] = "tf.keras"

from keras.models import load_model
import segmentation_models as sm
import numpy as np

import numpy as np
import skimage.io as io
import random
import os,sys
import glob
import cv2
import json
import pydash as pyd
from tensorflow.keras.preprocessing.image import ImageDataGenerator

import segmentation_models as sm

sys.path.append("..")
from utils.image_util import *
from utils.augmentation import *
from utils.data_loader import Dataset, Dataloader
from utils.image_util import *

parser = argparse.ArgumentParser(description='Semantic segmentation of IDCard in Image.')
parser.add_argument('input', type=str, help='Image (with IDCard) Input file')
parser.add_argument('--output_mask', type=str, default='output_mask.png', help='Output file for mask')
parser.add_argument('--output_prediction', type=str, default='output_pred.png', help='Output file for image')
parser.add_argument('--model', type=str, default='model.h5', help='Path to .h5 model file')

args = parser.parse_args()

INPUT_FILE = args.input
OUTPUT_MASK = args.output_mask
OUTPUT_FILE = args.output_prediction
MODEL_FILE = args.model

def load_image(preprocessing):
    img = cv2.imread(INPUT_FILE)
    img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
    
    height, width = img.shape[:2]
    img = cv2.resize(img, (320, 320), interpolation=cv2.INTER_AREA)
    mask = np.zeros((320, 320, 1), dtype=np.uint8)

    sample = preprocessing(image=img, mask=mask)
    return sample['image'], height, width

def predict_image(model, image, width, height):
    image = np.expand_dims(image, axis=0)
    mask = model.predict(image)

    ktp_mask = cv2.resize(mask[..., 1].squeeze(), (width, height), interpolation=cv2.INTER_AREA)*255
    foto_mask = cv2.resize(mask[..., 2].squeeze(), (width, height), interpolation=cv2.INTER_AREA)*255

    ktp_mask = cv2.threshold(ktp_mask, 200, 255, cv2.THRESH_BINARY)[1].astype(np.uint8)
    foto_mask = cv2.threshold(foto_mask, 200, 255, cv2.THRESH_BINARY)[1].astype(np.uint8)

    return ktp_mask, foto_mask


def main():
    if not os.path.isfile(INPUT_FILE):
        print('Input image not found ', INPUT_FILE)
    else:
        if not os.path.isfile(MODEL_FILE):
            print('Model not found ', MODEL_FILE)

        else:
            BACKBONE = 'efficientnetb3'
            preprocess_input = sm.get_preprocessing(BACKBONE)
            classes = ['ktp', 'foto']
            n_classes = 1 if len(classes) == 1 else (len(classes) + 1)
            activation ='softmax'
            LR = 0.0001

            print('Load model... ', MODEL_FILE)
            model = sm.Unet(BACKBONE,classes=n_classes, activation=activation)
            model.load_weights(MODEL_FILE)

            print('Load image... ', INPUT_FILE)
            img, h, w = load_image(get_preprocessing(preprocess_input))

            print('Prediction...')
            ktp_mask, foto_mask = predict_image(model, img, w, h)

            print('Save output files...', OUTPUT_FILE)
            cv2.imwrite('ktp_' + OUTPUT_MASK, ktp_mask)
            cv2.imwrite('foto_' + OUTPUT_MASK, foto_mask)

            print('Cut it out...')
            ktp_warped, pts, _ = convert_object(cv2.imread(INPUT_FILE), ktp_mask)
            foto_warped, pts, _ = convert_object(cv2.imread(INPUT_FILE), foto_mask)
            
            cv2.imwrite('ktp_' + OUTPUT_FILE, ktp_warped)
            cv2.imwrite('foto_' + OUTPUT_FILE, foto_warped)

            print('Done.')


if __name__ == '__main__':
    main()
