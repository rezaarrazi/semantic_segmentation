#https://github.com/KMKnation/Four-Point-Invoice-Transform-with-OpenCV/blob/master/four_point_object_extractor.py

import cv2
import numpy as np

def order_points(pts):
    rect = np.zeros((4, 2), dtype="float32")

    s = pts.sum(axis=1)
    rect[0] = pts[np.argmin(s)]
    rect[2] = pts[np.argmax(s)]

    diff = np.diff(pts, axis=1)
    rect[1] = pts[np.argmin(diff)]
    rect[3] = pts[np.argmax(diff)]

    return rect

def get_max_width_height(tl, tr, br, bl):
    
    widthA = np.sqrt(((br[0] - bl[0]) ** 2) + ((br[1] - bl[1]) ** 2))
    widthB = np.sqrt(((tr[0] - tl[0]) ** 2) + ((tr[1] - tl[1]) ** 2))
    maxWidth = max(int(widthA), int(widthB))

    heightA = np.sqrt(((tr[0] - br[0]) ** 2) + ((tr[1] - br[1]) ** 2))
    heightB = np.sqrt(((tl[0] - bl[0]) ** 2) + ((tl[1] - bl[1]) ** 2))
    maxHeight = max(int(heightA), int(heightB))

    return maxWidth, maxHeight

def four_point_transform_photo(image, pts):
    rect = order_points(pts)
    maxWidth = 508
    maxHeight = 327

    dst = np.array([[365,  78],
                    [485,  78],
                    [485, 235],
                    [365, 235]], dtype="float32")

    M = cv2.getPerspectiveTransform(rect, dst)
    warped = cv2.warpPerspective(image, M, (maxWidth, maxHeight))
    return warped

def get_destination_points(rect):
    (tl, tr, br, bl) = rect
    maxWidth, maxHeight = get_max_width_height(tl, tr, br, bl)

    dst = np.array([
        [tl[0], tl[1]],
        [tl[0] + (maxWidth - 1), tl[1]],
        [tl[0] + (maxWidth - 1), tl[1] + (maxHeight - 1)],
        [tl[0], tl[1] + (maxHeight - 1)]], dtype="float32")
    
    return dst

def get_destination_points_crop(rect, resize=None, padding=None):
    (tl, tr, br, bl) = rect
    maxWidth, maxHeight = get_max_width_height(tl, tr, br, bl)
    if resize:
        maxHeight, maxWidth = resize

    dst = np.array([
        [0, 0],
        [(maxWidth - 1), 0],
        [(maxWidth - 1), (maxHeight - 1)],
        [0, (maxHeight - 1)]], dtype="float32")

    if padding:
        dst, maxWidth, maxHeight = add_padding(dst, maxHeight, maxWidth, padding)
    
    return dst, maxHeight, maxWidth

def add_padding(rect, maxHeight, maxWidth, padding):
    (tl, tr, br, bl) = rect
    
    new_rect =  np.array([
                    [padding+tl[0], padding+tl[1]],
                    [tr[0]+padding, tr[1]+padding],
                    [br[0]+padding, br[1]+padding],
                    [padding+bl[0], padding+bl[1]]], dtype="float32")

    maxWidth += (2*padding)
    maxHeight += (2*padding)

    return new_rect, maxWidth, maxHeight

def four_point_transform(image, pts, resize=None, padding=None, crop=False):
    rect = order_points(pts)

    maxHeight, maxWidth = image.shape[:2]
    dst = None
    if crop:
        dst, maxHeight, maxWidth = get_destination_points_crop(rect, resize, padding)
    else:
        dst = get_destination_points(rect)

    M = cv2.getPerspectiveTransform(rect, dst)
    warped = cv2.warpPerspective(image, M, (maxWidth, maxHeight))
    return warped, dst

def findLargestCountours(cntList, cntWidths):
    newCntList = []
    newCntWidths = []

    first_largest_cnt_pos = cntWidths.index(max(cntWidths))

    return cntList[first_largest_cnt_pos], cntWidths[first_largest_cnt_pos]

def get_contours(mask):
    gray = mask.copy()
    mask_ = np.zeros(mask.shape, dtype=np.uint8)
    # gray = cv2.bilateralFilter(gray, 11, 17, 17)
    # gray = cv2.medianBlur(gray, 5)
    countours, _ = cv2.findContours(gray, cv2.RETR_LIST, cv2.CHAIN_APPROX_NONE)

    for cnt in countours:
        peri = cv2.arcLength(cnt, True)
        convex_hull = cv2.convexHull(cnt)
        cv2.drawContours(mask_, [convex_hull], 0, (255), -1)

    countours, _ = cv2.findContours(mask_, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
    cnts = sorted(countours, key=cv2.contourArea, reverse=True)
    screenCntList = []
    scrWidths = []

    for cnt in cnts:
        convex_hull = cv2.convexHull(cnt)
        peri = cv2.arcLength(convex_hull, True)
        approx = cv2.approxPolyDP(convex_hull, 0.02 * peri, True)
        screenCnt = approx
        if (len(screenCnt) == 4) & (cv2.contourArea(screenCnt)>10):
            (X, Y, W, H) = cv2.boundingRect(screenCnt)
            screenCntList.append(screenCnt)
            scrWidths.append(W)
    
    return screenCntList, scrWidths

def convert_object(image, mask, resize=None, padding=None):
    contour_list, contour_width_list = get_contours(mask)
            
    if len(contour_width_list) == 0:
        print('ID Card not found.')
        return None, None, None
    else:
        largest_contour, largest_contour_width = findLargestCountours(contour_list, contour_width_list)

        pts = largest_contour.reshape(4, 2)
        
        display = image.copy()
        for pt in pts:
            cv2.circle(display, (pt[0],pt[1]), 3, (0,0,255), -1)

        warped, dst = four_point_transform(image, pts, resize, padding, crop=True)

        return warped, dst, display
