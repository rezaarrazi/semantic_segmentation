from pycocotools.coco import COCO
import numpy as np
import skimage.io as io
import random
import os, warnings
import cv2
from tqdm import tqdm

def filterDataset(annFile, classes=None):    
    # initialize COCO api for instance annotations
    coco = COCO(annFile)
    
    images = []
    if classes!=None:
        # iterate for each individual class in the list
        for className in classes:
            # get all images containing given categories
            catIds = coco.getCatIds(catNms=className)
            imgIds = coco.getImgIds(catIds=catIds)
            images += coco.loadImgs(imgIds)
    
    else:
        imgIds = coco.getImgIds()
        images = coco.loadImgs(imgIds)
    
    # Now, filter out the repeated images
    unique_images = []
    for i in range(len(images)):
        if images[i] not in unique_images:
            unique_images.append(images[i])
            
    random.shuffle(unique_images)
    dataset_size = len(unique_images)
    
    return unique_images, dataset_size, coco

def getClassName(classID, cats):
    for i in range(len(cats)):
        if cats[i]['id']==classID:
            return cats[i]['name']
    return None

def getImage(imageObj, img_folder):
    # Read and normalize an image
    train_img = cv2.imread(img_folder + '/' + imageObj['file_name'])
    # io.imread()/255.0

    if (len(train_img.shape)==3 and train_img.shape[2]==3): # If it is a RGB 3 channel image
        return train_img
    else: # To handle a black and white image, increase dimensions to 3
        stacked_img = np.stack((train_img,)*3, axis=-1)
        return stacked_img
    
def annToMaks(ann, input_image_size):
    mask = np.zeros(input_image_size, dtype=np.uint8)

    for segmentation in ann['segmentation']:
        points = [segmentation[x:x+2] for x in range(0, len(segmentation),2)]
        try:
            coords = np.array([[xy] for xy in points], dtype=np.int32)
            cv2.drawContours(mask, [coords], -1, (1), -1)
        except:
            raise warnings.warn("cannot convert to int {}".format(segmentation))
    # height, width = mask.shape[:2]
    
    return mask

def getNormalMask(imageObj, classes, coco, catIds, input_image_size):
    annIds = coco.getAnnIds(imageObj['id'], catIds=catIds, iscrowd=0)
    anns = coco.loadAnns(annIds)
    cats = coco.loadCats(catIds)
    train_mask = np.zeros(input_image_size, dtype=np.uint8)
    
    for a in range(len(anns)):
        className = getClassName(anns[a]['category_id'], cats)
        pixel_value = classes.index(className)+1
        new_mask = annToMaks(anns[a], input_image_size)*pixel_value
        train_mask = np.maximum(new_mask, train_mask)

    train_mask = train_mask // len(classes)
    
    # Add extra dimension for parity with train_img size [X * X * 3]
    train_mask = train_mask.reshape(input_image_size[0], input_image_size[1], 1)
    return train_mask
    
def getBinaryMask(imageObj, coco, catIds, input_image_size):
    annIds = coco.getAnnIds(imageObj['id'], catIds=catIds, iscrowd=0)
    anns = coco.loadAnns(annIds)
    train_mask = np.zeros(input_image_size, dtype=np.uint8)
    for a in range(len(anns)):
        new_mask = annToMaks(anns[a], input_image_size)
        
        #Threshold because resizing may cause extraneous values
        new_mask[new_mask >= 0.5] = 1
        new_mask[new_mask < 0.5] = 0

        train_mask = np.maximum(new_mask, train_mask)

    # Add extra dimension for parity with train_img size [X * X * 3]
    train_mask = train_mask.reshape(input_image_size[0], input_image_size[1], 1)
    return train_mask

def getAnnotation(imageObj, coco, catIds):
    annIds = coco.getAnnIds(imageObj['id'], catIds=catIds, iscrowd=None)
    anns = coco.loadAnns(annIds)
    cats = coco.loadCats(catIds)

    annotations = {}
    for a in range(len(anns)):
        className = getClassName(anns[a]['category_id'], cats)
        annotations['className'] = anns[a]

    return annotations 

def annotationCoco(images, classes, coco, folders):
    
    dataset_size = len(images)
    catIds = coco.getCatIds(catNms=classes)
    
    images_arr = [None]*dataset_size
    annotation_arr = [None]*dataset_size
    image_id_arr = [None]*dataset_size

    for i in range(dataset_size):
        imageObj = images[i]
        
        img_folder = ''
        for dir_ in folders:
            if os.path.isfile('{}/{}'.format(dir_,imageObj['file_name'])):
                img_folder = dir_
                break
                
        ### Retrieve Image ###
        train_img = getImage(imageObj, img_folder, None)

        annotation = getAnnotation(imageObj, coco, catIds)

        # Add to respective batch sized arrays
        images_arr[i] = train_img
        image_id_arr[i] = imageObj['id']
        annotation_arr[i] = annotation
        
    return images_arr, annotation_arr, image_id_arr

def dataGeneratorCoco(images, classes, coco, folders, 
                      input_image_size=None, mask_type='binary', include_zero=False):
    
    dataset_size = len(images)
    catIds = coco.getCatIds(catNms=classes)
    
    images_arr = []#[None]*dataset_size #np.zeros((dataset_size, input_image_size[0], input_image_size[1], 3), dtype=np.uint8)
    masks_arr = []#[None]*dataset_size #np.zeros((dataset_size, input_image_size[0], input_image_size[1], 1), dtype=np.uint8)
    image_id_arr = []#[None]*dataset_size

    print('get dataset')
    with tqdm(total=dataset_size) as pbar:
        for i in range(dataset_size):
            imageObj = images[i]
            
            img_folder = ''
            for dir_ in folders:
                if os.path.isfile('{}/{}'.format(dir_,imageObj['file_name'])):
                    img_folder = dir_
                    break
                    
            ### Retrieve Image ###
            train_img = getImage(imageObj, img_folder)
            
            ## Create Mask ###
            if mask_type=="binary":
                train_mask = getBinaryMask(imageObj, coco, catIds, train_img.shape[:2])
            
            elif mask_type=="normal":
                train_mask = getNormalMask(imageObj, classes, coco, catIds, train_img.shape[:2])                
            
            # Resize
            if input_image_size!=None:
                train_img = cv2.resize(train_img, input_image_size)
                train_mask = cv2.resize(train_mask, input_image_size)
                
            save = True
            if (not include_zero) and (cv2.countNonZero(train_mask)==0):
                save = False

            if save:
                # Add to respective batch sized arrays
                images_arr.append(train_img)
                image_id_arr.append(imageObj['file_name'])
                masks_arr.append(train_mask)

            pbar.update(1)
        
    return images_arr, masks_arr, image_id_arr