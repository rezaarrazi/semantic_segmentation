import cv2
import json
import numpy as np
import os, sys
import random
import re
import shutil
import wget
import zipfile
from PIL import Image
from glob import glob
import pydash as _
from tqdm import tqdm

sys.path.append("..")
from utils.pycoco import filterDataset, dataGeneratorCoco
from utils.augmentation import *

os.environ['DATASETS_DIR'] = 'D:\\Reza\\Dokumen\\datasets\\'

DATA_TRAIN_DIR = os.path.join(os.environ['DATASETS_DIR'] , 'coco\\train2017\\')
DATA_VAL_DIR = os.path.join(os.environ['DATASETS_DIR'] , 'coco\\val2017\\')

ANOTATION_TRAIN_FILE = os.path.join(os.environ['DATASETS_DIR'] , 'coco\\annotations\\instances_train2017.json')
ANOTATION_VAL_FILE = os.path.join(os.environ['DATASETS_DIR'] , 'coco\\annotations\\instances_val2017.json')

CLASSES = ['person']

TEMP_PATH = 'data/temp/'
TEMP_IMAGE_PATH = TEMP_PATH + 'image/'
TEMP_MASK_PATH = TEMP_PATH + 'mask/'

DATA_PATH = 'data/'

SEED = 230

def load_data():
    # Remove Temp Directory and create a new one
    if os.path.exists(DATA_PATH):
        print('Remove Temp Directory and create a new one')
        shutil.rmtree(DATA_PATH, ignore_errors=True)

    # Create folders to hold images and masks
    folders = ['train/image', 'train/mask', 'val/image', 'val/mask']

    for folder in folders:
        os.makedirs(DATA_PATH + folder)

    input_image_size = (320,320)
    mask_type = 'normal'

    #DATA VAL
    print('DATA VAL')
    images, dataset_size, coco = filterDataset(ANOTATION_VAL_FILE, CLASSES)
    images_arr, masks_arr, image_id_arr = dataGeneratorCoco(images, CLASSES, coco, [DATA_VAL_DIR],
                                input_image_size, mask_type)

    print('save dataset')
    with tqdm(total=len(images_arr)) as pbar:
        for img, mask, img_index in zip(images_arr, masks_arr, image_id_arr):
            mask = mask*255

            filename = img_index.split('.')[0] + '.png'
            cv2.imwrite(os.path.join(DATA_PATH , 'val/image/', filename), img)
            cv2.imwrite(os.path.join(DATA_PATH , 'val/mask/', filename), mask)
            
            pbar.update(1)

    #DATA TRAIN
    print('DATA TRAIN')
    images, dataset_size, coco = filterDataset(ANOTATION_TRAIN_FILE, CLASSES)
    images_arr, masks_arr, image_id_arr = dataGeneratorCoco(images, CLASSES, coco, [DATA_TRAIN_DIR],
                                input_image_size, mask_type)

    print('save dataset')
    with tqdm(total=len(images_arr)) as pbar:
        for img, mask, img_index in zip(images_arr, masks_arr, image_id_arr):
            mask = mask*255

            filename = img_index.split('.')[0] + '.png'
            cv2.imwrite(os.path.join(DATA_PATH , 'train/image/', filename), img)
            cv2.imwrite(os.path.join(DATA_PATH , 'train/mask/', filename), mask)
            
            pbar.update(1)

if __name__ == '__main__':
    load_data()
