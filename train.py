import numpy as np
import os, sys
import random as rn
import tensorflow as tf
import time

os.environ["SM_FRAMEWORK"] = "tf.keras"

from tensorflow.keras.callbacks import EarlyStopping
from tensorflow.keras.callbacks import ModelCheckpoint
from tensorflow.keras.callbacks import ReduceLROnPlateau
from tensorflow.keras.callbacks import TensorBoard
from tensorflow.keras.optimizers import Adam
from tensorflow.keras.preprocessing.image import ImageDataGenerator
from tensorflow.keras.models import load_model

from utils.data_loader import Dataset, Dataloader
from utils.augmentation import *

import argparse
import segmentation_models as sm
from tensorflow.python.keras import backend as K

# Tensorflow session
config = tf.ConfigProto()
config.gpu_options.allow_growth = True  # dynamically grow the memory used on the GPU
config.log_device_placement = False  # to log device placement (on which device the operation ran)
sess = tf.Session(config=config)
K.set_session(sess)  # set this TensorFlow session as the default session for Keras

#ArgParse
def parse_arguments(args):
    parser = argparse.ArgumentParser(description='UNET')
    parser.add_argument('--train_dir', type=str, default='./data/train/', help='Train dir - with subdirs images ans masks')
    parser.add_argument('--val_dir', type=str, default='./data/val/', help='Val dir - with subdirs images ans masks')
    parser.add_argument('--result_dir', type=str,default="model", help='Result dir - where the model will be saved')
    parser.add_argument('--image_size', type=int,default=320, help='Image size - for cropping the images to nxn images')
    parser.add_argument('--image_channels', type=int,default=3, help='Image channels - number of channels of the input image')
    parser.add_argument('--padding_size', type=int,default=800, help='Padding size for Val images - must be a multiple of image size')
    parser.add_argument('--batch_size', type=int,default=4, help='Batch size')
    parser.add_argument('--epochs', type=int,default=100, help='# of Epochs')
    parser.add_argument('--model', type=str, help='Path to model checkpoint file')
    
    return parser.parse_args(args)

if __name__ == '__main__':

    args = parse_arguments(sys.argv[1:])

    # Directorios
    train_dir = args.train_dir
    test_dir = args.val_dir
    result_dir = args.result_dir

    if not os.path.exists(result_dir):
        os.mkdir(result_dir)

    x_train_dir = os.path.join(train_dir, 'image')
    y_train_dir = os.path.join(train_dir, 'mask')
    x_valid_dir = os.path.join(test_dir, 'image')
    y_valid_dir = os.path.join(test_dir, 'mask')

    MODEL_FILE = args.model

    #Model parameters
    BACKBONE = 'efficientnetb3'
    BATCH_SIZE = args.batch_size
    LR = 0.0005
    EPOCHS = args.epochs

    # define network parameters 
    classes = ['person']
    n_classes = 1 if len(classes) == 1 else (len(classes) + 1)  # case for binary and multiclass segmentation
    activation = 'sigmoid' if n_classes == 1 else 'softmax'
    image_size = args.image_size
    image_channels = args.image_channels
    padding_size = args.padding_size
    preprocess_input = sm.get_preprocessing(BACKBONE)

    # Dataset for train images
    train_dataset = Dataset(
        x_train_dir, 
        y_train_dir, 
        classes=classes,
        preprocessing=get_preprocessing(preprocess_input),
    )

    # Dataset for validation images
    valid_dataset = Dataset(
        x_valid_dir, 
        y_valid_dir, 
        classes=classes,
        preprocessing=get_preprocessing(preprocess_input),
    )

    model = None

    # define optomizer
    optim = Adam(LR)

    # Segmentation models losses can be combined together by '+' and scaled by integer or float factor
    dice_loss = sm.losses.DiceLoss() 
    focal_loss = sm.losses.BinaryFocalLoss() if n_classes == 1 else sm.losses.CategoricalFocalLoss()
    total_loss = dice_loss + (1 * focal_loss)

    metrics = [sm.metrics.IOUScore(threshold=0.5), sm.metrics.FScore(threshold=0.5)]

    #create model
    model = sm.Unet(BACKBONE, classes=n_classes, activation=activation, encoder_weights='imagenet')

    if not (MODEL_FILE is None):
        print("[INFO] loading {}...".format(MODEL_FILE))
        model = sm.Unet(BACKBONE,classes=n_classes, activation=activation)

        model.load_weights(MODEL_FILE)

    # compile keras model with defined optimozer, loss and metrics
    model.compile(optim, total_loss, metrics)

    #Dataloaderss
    train_dataloader = Dataloader(train_dataset, batch_size=BATCH_SIZE, shuffle=True)
    valid_dataloader = Dataloader(valid_dataset, batch_size=1, shuffle=False)

    # check shapes for errors
    assert train_dataloader[0][0].shape == (BATCH_SIZE, image_size, image_size, image_channels)
    assert train_dataloader[0][1].shape == (BATCH_SIZE, image_size, image_size, n_classes)

    # define callbacks for learning rate scheduling and best checkpoints saving
    callbacks = [
        ModelCheckpoint(os.path.join(result_dir,'model.h5'), save_weights_only=True, save_best_only=True, monitor='val_loss', mode='min', verbose=1),
        EarlyStopping(patience=10, verbose=1, monitor='val_loss', mode='min')
    ]

    # train model
    history = model.fit_generator(
        train_dataloader, 
        steps_per_epoch=len(train_dataloader), 
        epochs=EPOCHS, 
        callbacks=callbacks, 
        validation_data=valid_dataloader, 
        validation_steps=len(valid_dataloader),
    )